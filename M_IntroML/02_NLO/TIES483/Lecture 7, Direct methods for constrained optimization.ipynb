{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"font-family: Arial,sans-serif; color: #3070b3; font-size: 13px; line-height: 14px; margin-top:25px; \">\n",
    "    <div style=\"float:right;\">\n",
    "        <a href=\"https://www.asg.ed.tum.de/en/lmf\" title=\"Home\">\n",
    "            <img src=\"https://upload.wikimedia.org/wikipedia/commons/c/c8/Logo_of_the_Technical_University_of_Munich.svg\" alt=\"Technical University of Munich\" style=\"height: 42px\">\n",
    "        </a>\n",
    "    </div>\n",
    "    <div>\n",
    "        <ul style=\"padding: 0pt; margin: 0pt; list-style-type: none;\">\n",
    "            <li> <a href=\"https://www.asg.ed.tum.de/en/lmf/\" title=\"Home Chair of Remote Sensing Technology\" style=\"text-decoration: none;\">Chair of Remote Sensing Technology</a></li>\n",
    "            <li> <a href=\"https://www.ed.tum.de/en/\" title=\"Home TUM School of Engineering and Design\" style=\"text-decoration: none;\">TUM School of Engineering and Design</a></li>\n",
    "            <li> <a href=\"https://www.tum.de/en/\" title=\"Home Technical University of Munich\" style=\"text-decoration: none;\"> Technical University of Munich </a></li>\n",
    "        </ul>\n",
    "    </div>\n",
    "</div>\n",
    "\n",
    "<div style=\"font-family: Arial,sans-serif; font-size: 14px; line-height: 16px; margin-top:50px\">\n",
    "    <div style=\"float:right;\">\n",
    "        <span style=\"font-size:small\">Prof. Dr.-Ing. habil.</span><br />\n",
    "        <b>Richard Bamler</b><br /><br />\n",
    "        <span style=\"font-size:small\">Prof. Dr. rer. nat. habil.</span><br />\n",
    "        <b>Marco Körner</b><br />\n",
    "    </div>\n",
    "    <div style=\"\">\n",
    "        <h1>Remote Sensing Data <span style=\"font-size:small\"> or &nbsp; </span> Estimation Theory</h1>\n",
    "        <p style=\"font-size:large\">Summer Term 2024</p>\n",
    "    </div>\n",
    "</div>\n",
    "\n",
    "|||\n",
    "|:---|:---|\n",
    "|Note |_The following notebook has been excerpted from the fantastic collection authored by [Markus Hartikainen](https://converis.jyu.fi/converis/portal/detail/Person/4429256?lang=en_GB) from [Jyväskylä University](https://converis.jyu.fi/converis/portal/overview?lang=en_GB), which you can also find on his [GitHub](https://github.com/maeehart/TIES483) repository. I added a few more commands and replaced some older resources that are no longer compatible with contemporary Python environments._ |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lecture 7, direct methods for constrained optimization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Direct methods for constrained optimization are also known as *methods of feasible directions*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Feasible descent directions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let $S\\subset \\mathbb R^n$ ($S\\neq \\emptyset$ closed) and $x^*\\in S$. \n",
    "**Definition:** The set\n",
    "$$ D = \\{d\\in \\mathbb R^n: d\\neq0,x^*+\\alpha d\\in S \\text{ for all } \\alpha\\in (0,\\delta) \\text{ for some } \\delta>0\\}$$\n",
    "is called the cone of feasible directions of $S$ in $x^*$.\n",
    "\n",
    "**Definition:** The set \n",
    "$$ F = \\{d\\in \\mathbb R^n: f(x^*+\\alpha d)<f(x^*)\\text{ for all } \\alpha\\in (0,\\delta) \\text{ for some } \\delta>0\\}$$\n",
    "is called the cone of descent directions.\n",
    "\n",
    "**Definition:** The set $F\\cap D$ is called the cone of feasible descent directions.\n",
    "\n",
    "![alt text](images/feasible_descent_directions.svg \"Feasible descent directions\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**(Obvious) Theorem:** Consider an optimization problem \n",
    "$$\n",
    "\\begin{align}\n",
    "\\min &\\  f(x)\\\\\n",
    "\\text{s.t. }&\\ x\\in S\n",
    "\\end{align}\n",
    "$$\n",
    "and let $x^*\\in S$. Now if $x^*$ is a local minimizer **then** the set of feasible descent directions $F\\cap D$ is empty."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Idea for the methods of feasible descent directions\n",
    "\n",
    "1. Assume a feasible solution $x$.\n",
    "2. Find a feasible descent direction $d\\in D\\cap F$.\n",
    "3. Determine the step length to the direction $d$\n",
    "4. Update $x$ accordingly.\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Rosen's projected gradient method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Assume a problem with linear equality constraints\n",
    "$$\n",
    "\\min f(x)\\\\\n",
    "\\text{s.t. }Ax=b,\n",
    "$$\n",
    "where $A$ is a $lxn$ matrix ($l\\leq n$) and $b$ is a vector."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Let $x$ be a feasible solution to the above problem.\n",
    "\n",
    "It holds that:\n",
    "\n",
    "$d$ is a feasible direction *if and only if* $Ad=0$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Thus, the gradient $\\nabla f(x)$ is a feasible descent direction, if \n",
    "$$ A\\nabla f(x)=0.$$\n",
    "\n",
    "This may or may not be true.\n",
    "\n",
    "However, we can project the gradient to the set of feasible descent directions\n",
    "$$ \\{d\\in \\mathbb R^n: Ad=0\\},$$\n",
    "which now is a linear subspace."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![alt text](images/subspace.svg \"A linear subspace Ad=0\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Projection\n",
    "\n",
    "Let $a\\in \\mathbb R^n$ be a vector and let $L$ be a linear subspace of $\\mathbb R^n$. Now, the following are equivalent\n",
    "* $a^P$ is the projection of $a$ on $L$,\n",
    "* $\\{a^P\\} = \\operatorname{argmin}_{l\\in L}\\|a-l\\|$, and\n",
    "* $a^P\\in A$ and $(a-a^P)^Tl=0$ for all $l\\in L$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Projected gradient"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The projection of the gradient $\\nabla f(x)$ on the set $\\{d\\in \\mathbb R^n: Ad=0\\}$ is denoted by $\\nabla f(x)^P$ and called the *projected gradient*. \n",
    "\n",
    "Now, given some conditions, the projected gradient gives us a feasible descent direction."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![alt text](images/projected_gradient.svg \"A projected gradient\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## How to compute the projected gradient?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are different ways, but at this course we can use optimization. Basically, the optimization problem that we have to solve is\n",
    "$$\n",
    "\\min \\|\\nabla f(x)-d\\|\\\\\n",
    "\\text{s.t. }Ad=0.\n",
    "$$\n",
    "\n",
    "Since it is equivalent to minimize the square of the objective function $\\sum_{i=n}\\nabla_i f(x)^2+d_i^2-2\\nabla_i f(x)d_i$, we can see that the problem is a quadratic problem with inequality constraints,\n",
    "$$\n",
    "\\min \\frac12 d^TId-\\nabla f(x)^Td\\\\\n",
    "\\text{s.t. }Ad=0\n",
    "$$\n",
    "which means that we just need to solve the system of equations (see e.g., https://en.wikipedia.org/wiki/Quadratic_programming#Equality_constraints)\n",
    "$$\n",
    "\\left[\n",
    "\\begin{array}{cc}\n",
    "I&A^T\\\\\n",
    "A&0\n",
    "\\end{array}\n",
    "\\right] \n",
    "\\left[\\begin{align}d\\\\\\lambda\\end{align}\\right]\n",
    "= \\left[ \n",
    "\\begin{array}{c}\n",
    "\\nabla f(x)\\\\\n",
    "0\n",
    "\\end{array}\n",
    "\\right],\n",
    "$$\n",
    "where I is the identity matrix, and $\\lambda$ are the KKT multipliers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Code in Python"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### A function for projecting a vector to a linear space defined by $Ax=0$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "def project_vector(A,vector):\n",
    "    #convert A into a matrix\n",
    "    A_matrix = np.matrix(A)\n",
    "    #construct the \"first row\" of the matrix [[I,A^T],[A,0]]\n",
    "    left_matrix_first_row = np.concatenate((np.identity(len(vector)),A_matrix.transpose()), axis=1)\n",
    "    #construct the \"second row\" of the matrix\n",
    "    left_matrix_second_row = np.concatenate((A_matrix,np.matrix(np.zeros([len(A),len(vector)+len(A)-len(A[0])]))), axis=1)\n",
    "    #combine the whole matrix by combining the rows\n",
    "    left_matrix = np.concatenate((left_matrix_first_row,left_matrix_second_row),axis = 0)\n",
    "    #Solve the system of linear equalities from the previous page\n",
    "    return np.linalg.solve(left_matrix, \\\n",
    "                           np.concatenate((np.matrix(vector).transpose(),\\\n",
    "                                           np.zeros([len(A),1])),axis=0))[:len(vector)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "A = [[1,0,0],[0,1,0]]\n",
    "gradient = [1,1,1]\n",
    "project_vector(A,gradient)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us study optimization problem\n",
    "$$\n",
    "\\begin{align}\n",
    "\\min \\qquad& x_1^2+x_2^2+x_3^2\\\\\n",
    "\\text{s.t.}\\qquad &x_1+x_2=3\\\\\n",
    "    &x_1+x_3=4.\n",
    "\\end{align}\n",
    "$$\n",
    "Let us project a gradient from a feasible point $x=(1,2,3)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, the matrix\n",
    "$$\n",
    "A = \\left[\n",
    "\\begin{array}{ccc}\n",
    "1& 1 & 0\\\\\n",
    "1& 0 & 1\n",
    "\\end{array}\n",
    "\\right]\n",
    "$$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# import ad\n",
    "\n",
    "import jax.numpy as jnp\n",
    "import jax\n",
    "\n",
    "def gh(f):\n",
    "    gradx = jax.grad(f, allow_int=True)\n",
    "    hessx = jax.jacfwd(gradx)\n",
    "    return gradx, hessx\n",
    "\n",
    "A = [[1,1,0],[1,0,1]]\n",
    "gradient = gh(lambda x:x[0]**2+x[1]**2+x[2]**2)[0]([1.,2.,3.])\n",
    "d = project_vector(A,[-i for i in gradient])\n",
    "print(d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### d is a feasible direction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "np.matrix(A)*d"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### d is a descent direction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def f(x):\n",
    "    return x[0]**2+x[1]**2+x[2]**2\n",
    "\n",
    "alpha = 0.001\n",
    "print(f'Value of f at [1,2,3] is {f([1,2,3])}')\n",
    "x_mod= np.array([1,2,3])+alpha*np.array(d).transpose()[0]\n",
    "print(f'Value of f at [1,2,3] + alpha * d is {f(x_mod)}')\n",
    "print(f'Gradient dot product direction (i.e., directional derivative) is {np.matrix(gh(f)[0]([1.,2.,3.])).dot(np.array(d))}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Finally, the algorithm of the projected gradient"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "def projected_gradient_method(f,A,start,step,precision):\n",
    "    f_old = float('Inf')\n",
    "    x = np.array(start, float)\n",
    "    steps = []\n",
    "    f_new = f(x)\n",
    "    while abs(f_old-f_new)>precision:\n",
    "        f_old = f_new\n",
    "        gradient = gh(f)[0](x)\n",
    "        grad_proj = project_vector(A,[-i for i in gradient])#The only changes to steepest..\n",
    "        grad_proj = np.array(grad_proj.transpose())[0] #... descent are here!\n",
    "#        import pdb; pdb.set_trace()\n",
    "        x = x+grad_proj*step\n",
    "        f_new = f(x)\n",
    "        steps.append(list(x))\n",
    "    return x,f_new,steps"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "f = lambda x:x[0]**2+x[1]**2+x[2]**2\n",
    "A = [[1,1,0],[1,0,1]]\n",
    "start = [1,2,3]\n",
    "(x,f_val,steps) = projected_gradient_method(f,A,start,0.6,0.000001)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    },
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print(f'x            = {x}')\n",
    "print(f'f(x)         = {f(x)}')\n",
    "print(f'f({[1,2,3]}) = {f([1,2,3])}')\n",
    "print(f'Ax\\'          = {(np.matrix(A)*np.matrix(x).transpose()).transpose()}')"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
