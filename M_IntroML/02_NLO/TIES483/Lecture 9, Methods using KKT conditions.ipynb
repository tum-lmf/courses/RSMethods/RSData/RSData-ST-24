{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"font-family: Arial,sans-serif; color: #3070b3; font-size: 13px; line-height: 14px; margin-top:25px; \">\n",
    "    <div style=\"float:right;\">\n",
    "        <a href=\"https://www.asg.ed.tum.de/en/lmf\" title=\"Home\">\n",
    "            <img src=\"https://upload.wikimedia.org/wikipedia/commons/c/c8/Logo_of_the_Technical_University_of_Munich.svg\" alt=\"Technical University of Munich\" style=\"height: 42px\">\n",
    "        </a>\n",
    "    </div>\n",
    "    <div>\n",
    "        <ul style=\"padding: 0pt; margin: 0pt; list-style-type: none;\">\n",
    "            <li> <a href=\"https://www.asg.ed.tum.de/en/lmf/\" title=\"Home Chair of Remote Sensing Technology\" style=\"text-decoration: none;\">Chair of Remote Sensing Technology</a></li>\n",
    "            <li> <a href=\"https://www.ed.tum.de/en/\" title=\"Home TUM School of Engineering and Design\" style=\"text-decoration: none;\">TUM School of Engineering and Design</a></li>\n",
    "            <li> <a href=\"https://www.tum.de/en/\" title=\"Home Technical University of Munich\" style=\"text-decoration: none;\"> Technical University of Munich </a></li>\n",
    "        </ul>\n",
    "    </div>\n",
    "</div>\n",
    "\n",
    "<div style=\"font-family: Arial,sans-serif; font-size: 14px; line-height: 16px; margin-top:50px\">\n",
    "    <div style=\"float:right;\">\n",
    "        <span style=\"font-size:small\">Prof. Dr.-Ing. habil.</span><br />\n",
    "        <b>Richard Bamler</b><br /><br />\n",
    "        <span style=\"font-size:small\">Prof. Dr. rer. nat. habil.</span><br />\n",
    "        <b>Marco Körner</b><br />\n",
    "    </div>\n",
    "    <div style=\"\">\n",
    "        <h1>Remote Sensing Data <span style=\"font-size:small\"> or &nbsp; </span> Estimation Theory</h1>\n",
    "        <p style=\"font-size:large\">Summer Term 2024</p>\n",
    "    </div>\n",
    "</div>\n",
    "\n",
    "|||\n",
    "|:---|:---|\n",
    "|Note |_The following notebook has been excerpted from the fantastic collection authored by [Markus Hartikainen](https://converis.jyu.fi/converis/portal/detail/Person/4429256?lang=en_GB) from [Jyväskylä University](https://converis.jyu.fi/converis/portal/overview?lang=en_GB), which you can also find on his [GitHub](https://github.com/maeehart/TIES483) repository. I added a few more commands and replaced some older resources that are no longer compatible with contemporary Python environments._ |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Lecture 9: Methods using KKT conditions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Sequential Quadratic Programming (SQP)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Idea is to generate a sequence of quadratic optimization problems whose solutions approach the solution of the original problem**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us consider problem\n",
    "$$\n",
    "\\min f(x)\\\\\n",
    "\\text{s.t. }h_k(x) = 0\\text{ for all }k=1,\\ldots,K,\n",
    "$$\n",
    "where the the objective function and the equality constraints are twice differentiable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because we know that the optimal solution of this problem satisfies the KKT conditions, we know that\n",
    "$$\n",
    "\\left\\{\\begin{array}{l}\n",
    "\\nabla_xL(x,\\lambda,\\mu)=\\nabla f(x) + \\mu\\nabla h(x) = 0\\\\\n",
    "h(x) = 0\n",
    "\\end{array}\\right.\n",
    "$$\n",
    "Let us assume that we have a current esimation for the solution of the equality constraints $(x^k,\\mu^k)$, then according to the Newton's method for root finding (see e.g., https://en.wikipedia.org/wiki/Newton's_method), we have another solution $(x^k,\\mu^k)^T+(p,v)^T$ of the problem by solving system of equations\n",
    "$$\n",
    "HL(x,\\lambda,\\mu)\\left[\\begin{align}p^T\\\\v^T\\end{align}\\right] = -\\nabla L(x,\\lambda,\\mu).\n",
    "$$\n",
    "This can be written as\n",
    "$$\n",
    "\\left[\n",
    "\\begin{array}{cc}\n",
    "H_xL(x^k,\\lambda,\\mu^k)&\\nabla h(x^k)\\\\\n",
    "\\nabla h(x^k)^T & 0\n",
    "\\end{array}\n",
    "\\right]\n",
    "\\left[\\begin{array}{c}p^T\\\\v^T\\end{array}\\right] =\n",
    "\\left[\n",
    "\\begin{array}{c}\n",
    "-\\nabla_x L(x^k,\\lambda,\\mu^k)\\\\\n",
    "-h(x^k)^T\n",
    "\\end{array}\n",
    "\\right].\n",
    "$$\n",
    "\n",
    "However, the above is just the solution of the quadratic problem with equality constraints\n",
    "$$\n",
    "\\min \\frac12 p^TH_xL(x^k,\\lambda,\\mu^k)p+\\nabla_xL(x^k,\\lambda,\\mu^k)^Tp\\\\\n",
    "\\text{s.t. }h_j(x^k) + \\nabla h_j(x^k)^Tp = 0. \n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Intuitive interpretation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are approximating the function quadratically around the current solution and the constraints are approximated linearly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Implementation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Define an optimization problem, where\n",
    "* $f(x) = \\|x\\|^2$\n",
    "* $h(x) = \\sum_{i=1}^nx_i-n$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "# import ad\n",
    "import jax.numpy as jnp\n",
    "import jax"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "def f_constrained(x):\n",
    "    # return sum([i**2. for i in x]),[],[sum(x)-len(x),x[0]**2+x[1]-2]\n",
    "    return x[0],[],[1,x[0]**2+x[1]-2]\n",
    "    # return x[0], [],[1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "print(f_constrained([1,0,1]))\n",
    "print(f_constrained([1,2,3,4]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def gh(f):\n",
    "    gradx = jax.grad(f)\n",
    "    hessx = jax.jacfwd(gradx)\n",
    "    return gradx, hessx\n",
    "\n",
    "\n",
    "\n",
    "#if k=0, returns the gradient of lagrangian, if k=1, returns the hessian\n",
    "def diff_L(f,x,m,k):\n",
    "    #Define the lagrangian for given m and f\n",
    "    L = lambda x_: f(x_)[0] + (np.matrix(f(x_)[2])*np.matrix(m).transpose())[0,0]\n",
    "    \n",
    "    # return gh(L)[k](np.array(x,float))\n",
    "    \n",
    "    grad_hess_L = gh(L)\n",
    "    print(f'grad_hess_L: {type(grad_hess_L)}')\n",
    "    \n",
    "    deriv = grad_hess_L[k]\n",
    "    print(f'deriv: {type(deriv)}')\n",
    "    \n",
    "    X = jnp.array(x, float)\n",
    "    print(f'X: {type(X)}')\n",
    "    \n",
    "    grad = deriv(X)\n",
    "    print(f'grad: {type(grad)}')\n",
    "    \n",
    "    return grad\n",
    "    \n",
    "    # return gh(L)[k](np.array(x,float))\n",
    "\n",
    "#Returns the gradients of the equality constraints\n",
    "def grad_h(f,x):\n",
    "    return  [gh(lambda y: f(y)[2][i])[0](x) for i in range(len(f(x)[2]))] \n",
    "\n",
    "#Solves the quadratic problem inside the SQP method\n",
    "def solve_QP(f,x,m):\n",
    "    left_side_first_row = np.concatenate((\\\n",
    "    np.matrix(diff_L(f,x,m,1)),\\\n",
    "    np.matrix(grad_h(f,x)).transpose()),axis=1)\n",
    "    left_side_second_row = np.concatenate((\\\n",
    "    np.matrix(grad_h(f,x)),\\\n",
    "    np.matrix(np.zeros((len(f(x)[2]),len(f(x)[2]))))),axis=1)\n",
    "    right_hand_side = np.concatenate((\\\n",
    "    -1*np.matrix(diff_L(f,x,m,0)).transpose(),\n",
    "    -np.matrix(f(x)[2]).transpose()),axis = 0)\n",
    "    left_hand_side = np.concatenate((\\\n",
    "                                    left_side_first_row,\\\n",
    "                                    left_side_second_row),axis = 0)\n",
    "    temp = np.linalg.solve(left_hand_side,right_hand_side)\n",
    "    return temp[:len(x)],temp[len(x):]\n",
    "    \n",
    "    \n",
    "\n",
    "def SQP(f,start,precision):\n",
    "    x = start\n",
    "    m = np.ones(len(f(x)[2]))\n",
    "    f_old = float('inf')\n",
    "    f_new = f(x)[0]\n",
    "    while abs(f_old-f_new)>precision:\n",
    "        print(x)\n",
    "        f_old = f_new\n",
    "        (p,v) = solve_QP(f,x,m)\n",
    "        x = x+np.array(p.transpose())[0]\n",
    "        m = m+v\n",
    "        f_new = f(x)[0]\n",
    "    return x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x=[1., 2.]\n",
    "x=jnp.asarray(x)\n",
    "print(f_constrained(x)[0])\n",
    "print(f_constrained(x)[2])\n",
    "\n",
    "f = f_constrained\n",
    "# L = lambda x_: f(x_)[0] + (np.matrix(f(x_)[2])*np.matrix(m).transpose())[0,0]\n",
    "L = lambda x_: f(x_)[0] + (np.matrix(f(x_)[2])*np.matrix(m).transpose())[0,0]\n",
    "grad_hess_L = gh(L)\n",
    "deriv = grad_hess_L[0]\n",
    "grad = deriv(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "SQP(f_constrained,[0,0,0],0.0001)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Lagrangian methods -- \"The original method of multipliers\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us again consider problem\n",
    "$$\n",
    "\\min f(x)\\\\\n",
    "\\text{s.t. }h_k(x) = 0\\text{ for all }k=1,\\ldots,K,\n",
    "$$\n",
    "where the the objective function and the equality constraints are twice differentiable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Define augmented Lagrangian function\n",
    "$$\n",
    "L_c(x,\\mu) = f(x)+\\mu h(x)+\\frac12c\\|h(x)\\|^2.\n",
    "$$\n",
    "Above $c\\in \\mathbb R$ is a penalty parameter and $\\mu \\in \\mathbb R^n$ is a multiplier."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Let us consider sequence of optimization problems\n",
    "$$\n",
    "\\min_{x\\in\\mathbb R^n} f(x)+\\mu_k h(x)+\\frac{1}{2}c_k\\|h(x)\\|^2,\n",
    "$$\n",
    "where $c_{k+1}>c_k$ for $k=1,2,\\ldots$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, if $\\mu_k=0$ for all $k=1,2,\\ldots$, then we have a penalty function method, which solves the problem when $c_k\\to \\infty$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, it can be shown, that if we set $\\mu_0$ randomly and keep on updating\n",
    "$\\mu_{k+1} = \\mu_k-c_kh(x_k)$, then we can show that there exists $C>0$ such that of $c_k>C$, then the optimal solution of the augmented Langrangian solves the original problem!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us have optimization problem\n",
    "$$\n",
    "\\min x_1^2+x_2^2\\\\\n",
    "\\text{s.t. }x_1+x_2-1=0.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, the minimizatio of the augmented Lagrangian becomes\n",
    "$$\n",
    "\\min_{x\\in\\mathbb R^n} x_1^2+x_2^2+\\mu_k(x_1+x_2-1)+\\frac12c_k(x_1+x_2-1)^2.\\\\\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f_constrained2(x):\n",
    "    return sum([i**2 for i in x]),[],[sum(x)-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def augmented_langrangian(f,x,mu,c):\n",
    "    second_term = float(numpy.matrix(mu)*numpy.matrix(f(x)[2]).transpose())\n",
    "    third_term = 0.5*c*numpy.linalg.norm(f(x)[2])**2\n",
    "    return f(x)[0]-second_term+third_term"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "from scipy.optimize import minimize\n",
    "import numpy\n",
    "def augmented_langrangian_method(f,start,mu0,c0):\n",
    "    x_old = [float('inf')]*2\n",
    "    x_new = start\n",
    "    mu = mu0\n",
    "    c = c0\n",
    "    while numpy.linalg.norm(f(x_new)[2])>0.00001:\n",
    "        res = minimize(lambda x:augmented_langrangian(f,x,mu,c),x_new)\n",
    "        x_old = x_new\n",
    "        mu = float(mu-numpy.matrix(c)*numpy.matrix(f(res.x)[2]).transpose())\n",
    "        x_new = res.x\n",
    "        c = 2*c\n",
    "    return x_new,c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.optimize import minimize\n",
    "import numpy\n",
    "def penalty_function_method(f,start,c0):\n",
    "    x_old = [float('inf')]*2\n",
    "    x_new = start\n",
    "    c = c0\n",
    "    while numpy.linalg.norm(f(x_new)[2])>0.00001:\n",
    "        res = minimize(lambda x:augmented_langrangian(f,x,0,c),x_new)\n",
    "        x_old = x_new\n",
    "        x_new = res.x\n",
    "        c = 2*c\n",
    "    return x_new,c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "augmented_langrangian_method(f_constrained2,[0,0],1,1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "penalty_function_method(f_constrained2,[0,0],1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## What is going on in here?\n",
    "\n",
    "This is not completely trivial, unfortunately. If you want to read details, please see e.g., http://www.mit.edu/~dimitrib/Constrained-Opt.pdf."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
