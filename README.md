# Remote Sensing Data - ST 24

This repository contains some sample and exercise script for the 2024 courses on "Estimation Theory" or "Remote Sensing Data".

You can clone this repository and run in locally on your own machine. There is an `environment.yml` file that you can use to create a conda environment.

If you don't want to or can't run it locally, you can also create a MyBinder instance to run the notebooks remotely: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.lrz.de%2Ftum-lmf%2Fcourses%2Fenv%2Fbinder/main?urlpath=git-pull%3Frepo%3Dhttps%253A%252F%252Fgitlab.lrz.de%252Ftum-lmf%252Fcourses%252FRSMethods%252FRSData%252FRSData-ST-24%26urlpath%3Dlab%252Ftree%252FRSData-ST-24%252F%26branch%3Dmain) The setup of this environment might take a while to complete, just be patient.

## Usage Notes

These notebook might use some additional libraries that are not included in the environment file. In the headers of each notebook, these libraries are indicated. Just uncomment the installation prompts (`!pip ...` or `!conda ...`) at first use.

Feel free to make any modifications to the code and in particular to the parameter values used within.
